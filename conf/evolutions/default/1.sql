# Properties schema

# --- !Ups
create table properties (
  id SERIAL PRIMARY KEY,
  keywords TEXT NOT NULL,
  bathroom_number INT NOT NULL,
  bedroom_number INT NOT NULL,
  price_formatted TEXT NOT NULL,
  price_type TEXT NOT NULL,
  property_type TEXT NOT NULL
)

# --- !Downs
drop table properties