package actors

import akka.actor.Actor
import models.Properties
import play.api.Logger
import play.api.libs.json._
import play.api.libs.ws.WSClient
import play.api.libs.functional.syntax._
import services.PropertiesService

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class NestoriaFetchActor (ws: WSClient, ps: PropertiesService, url: String) extends Actor {

  def receive = {
    case "fetch-data" => fetchHouseInfo
    case _       => Logger.error("NestoriaFetchActor: Incorrect Operation")
  }

  implicit val propertiesReads: Reads[Properties] = (
    Reads.pure(Option(0)) and
      (__ \ "keywords").read[String] and
      (__ \ "bathroom_number").read[Int] and
      (__ \ "bedroom_number").read[Int] and
      (__ \ "price_formatted").read[String] and
      (__ \ "price_type").read[String] and
      (__ \ "property_type").read[String]
    )(Properties)

  def fetchHouseInfo {
    val futureRs: Future[List[Properties]] = ws.url(url)
      .withHeaders("Accept" -> "application/json")
      .withRequestTimeout(10000.millis).get().map {
      response => (response.json \ "response" \ "listings").as[List[Properties]]
    }
    futureRs onComplete {
      case Success(props) =>
        ps.updateData(props)
        Logger.info(s"NestoriaFetchActor: Data fetched successfully: [${props}]")
      case Failure(msg) => Logger.error("NestoriaFetchActor: An error occurred while trying to fetch the data")
    }
  }


}
