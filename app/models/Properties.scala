package models

import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.Future
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import scala.concurrent.ExecutionContext.Implicits.global

case class Properties(id: Option[Int], keywords: String, bathroomNum: Int, bedroomNum: Int, priceFormatted: String, priceType: String, propertyType: String)

class PropertiesTableDef(tag: Tag) extends Table[Properties](tag, "properties") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def keywords = column[String]("keywords")
  def bathroomNum = column[Int]("bathroom_number")
  def bedroomNum = column[Int]("bedroom_number")
  def priceFormatted = column[String]("price_formatted")
  def priceType = column[String]("price_type")
  def propertyType = column[String]("property_type")

  override def * =
    (id.?, keywords, bathroomNum, bedroomNum, priceFormatted, priceType, propertyType) <>(Properties.tupled, Properties.unapply)
}

object PropertiesRepo {

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val properties = TableQuery[PropertiesTableDef]

  def listAll: Future[Seq[Properties]] = {
    dbConfig.db.run(properties.result)
  }

  def updateData(props: List[Properties]): Future[Unit] = {
    val transaction = (for {
      _ <- properties.delete
      _ <- properties ++= props
    } yield ()).transactionally
    dbConfig.db.run(transaction)
  }
}