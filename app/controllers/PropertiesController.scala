package controllers

import javax.inject.Inject

import models.Properties
import play.api.libs.json._
import play.api.mvc._
import services.{NestoriaService, PropertiesService}

import scala.concurrent.ExecutionContext.Implicits.global


class PropertiesController @Inject()(nestoriaService: NestoriaService, propertiesService: PropertiesService) extends Controller {

  implicit val propertiesWrites = new Writes[Properties] {
    def writes(props: Properties) = Json.obj(
      "keywords" -> props.keywords,
      "bathroom_number" -> props.bathroomNum,
      "bedroom_number" -> props.bedroomNum,
      "price_formatted" -> props.priceFormatted,
      "price_type" -> props.priceType,
      "property_type" -> props.propertyType
    )
  }

  def getPropertiesByKeywords = Action.async { implicit request =>
    val json = request.body.asJson.get
    val keywords = json.as[List[String]]
    propertiesService.listMatching(keywords) map {
      response => Ok(Json.toJson(response))
    }
  }
}