package services

import models.{Properties, PropertiesRepo}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class PropertiesService {

  def listAll: Future[Seq[Properties]] = PropertiesRepo.listAll

  def updateData(props: List[Properties]): Future[Unit] = PropertiesRepo.updateData(props)

  def listMatching(keywords: List[String]): Future[Seq[Properties]] = {
    listAll map {
      response => {
        response.filter(stored => keywords.forall(stored.keywords.split(", ").contains))
      }
    }
  }
}
