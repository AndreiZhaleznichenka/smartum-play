package services

import javax.inject.Inject

import actors.NestoriaFetchActor
import akka.actor.ActorSystem
import akka.actor.Props
import play.api.Configuration
import play.api.libs.ws.WSClient

class NestoriaService @Inject()(playConfiguration: Configuration, ws: WSClient, ps: PropertiesService) {

  val nestoriaApi = playConfiguration.getString("api.nestoria").get

  val system = ActorSystem("NestoriaSystem")
  val nestoriaActor = system.actorOf(Props(new NestoriaFetchActor(ws, ps, nestoriaApi)), name = "nestoriaActor")
  nestoriaActor ! "fetch-data"
}
